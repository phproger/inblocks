@include('common.header')

<!-- Left side column. contains the logo and sidebar -->
@include('common.sidebar')  

<!-- Content Wrapper. Contains page content -->
@yield('content')
<!-- /.content-wrapper -->

@include('common.footer')  
