<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'image', 'url',  'user_id'];
    protected $dates =  ['deleted_at'];
    public function bannerGroups() {
    	return $this->belongsToMany('App\Models\BannerGroup', 'banner_group_banner')->withPivot('time','show_till','order');
  	}

}
