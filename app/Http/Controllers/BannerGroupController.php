<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\BannerGroup;
use Illuminate\Http\Request;

class BannerGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.banner_group.list', [
            'groups' => BannerGroup::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.banner_group.form', [
            'group' => new BannerGroup(),
            'banners' => Banner::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return view('pages.banner_group.form', [
            'group' => BannerGroup::find($id),
            'banners' => Banner::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == 0){
            $group = new BannerGroup();
        } else {
            $group = BannerGroup::find($id);
        }
        $group->fill($request->all());

        $group->save();

        return redirect(route('banner-groups.edit', $group->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = BannerGroup::withTrashed()->where('id', $id)->first();
        if (!$group->trashed()) {
            $group->delete();
        } else {
            $group->forceDelete();

        }
    }

    /**
     * Display a listing of the deleted banners
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        return view('pages.banner_group.list', [
            'groups' => BannerGroup::onlyTrashed()->get()]);
    }

    /**
     * Restore deleted banner.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $group = BannerGroup::withTrashed()->where('id', $id)->first();
        $group->restore();
    }

}
