<?php

/*
|---------------------------------------------------------------------		-----
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/logout', 'Auth\LoginController@logout')->name('logout.get');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@index')->name('homepage');

    Route::get('banners/trashed', 'BannerController@trashed')->name('banners.trashed');
    Route::post('banners/{id}/restore', 'BannerController@restore')->name('banners.restore');
    Route::resource('banners', 'BannerController');

    Route::get('banner-groups/trashed', 'BannerGroupController@trashed')->name('banner-groups.trashed');
    Route::post('banner-groups/{id}/restore', 'BannerGroupController@restore')->name('banner-groups.restore');
	Route::resource('banner-groups', 'BannerGroupController');

	Route::post('banners-save-order/{group_id}', ['as' => 'save-banners-order', 
												  'uses' =>'BannerGroupBannerController@saveBannersOrder']);

	Route::post('banner-save-data/{group_id}', ['as' => 'save-banner-data', 
												  'uses' =>'BannerGroupBannerController@saveBannerData']);

    Route::post('users/key/generate', 'UserController@generateKey')->name('users.generate_key');
    Route::get('users/{id}/delete', 'UserController@destroy')->name('users.delete');
	Route::resource('users', 'UserController');
});

