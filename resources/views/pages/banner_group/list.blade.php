@extends('common.index')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Список групп
                @if(Route::currentRouteName() == 'banner-groups.trashed')
                    <a href="{{route('banner-groups.index')}}">
                        <small>Корзина</small>
                    </a>
                @else
                    <a href="{{route('banner-groups.trashed')}}">
                        <small>Все</small>
                    </a>
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                @if(Route::currentRouteName() != 'banner-groups.trashed')
                    <li class="active">Список групп</li>
                @else
                    <li><a href="{{route('banner-groups.index')}}">Список групп</a></li>
                    <li class="active">Корзина</li>
                @endif
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!--  <div class="box-header">
                        </div> -->
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" class="token" value="{{csrf_token()}}">
                            <table id="main-table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Включенные баннеры</th>
                                    <th>Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($groups as $group)
                                    <tr data-id="{{$group->id}}">
                                        <td>{{$group->id}}</td>

                                        <td>{{$group->name}}</td>
                                        <td>
                                            @if($group->banners())
                                                @foreach($group->banners()->get() as $banner)
                                                    <a href="{{route('banners.edit', $banner->id)}}"
                                                       target="_blank">{{$banner->name}} </a> @if(!$loop->last) | @endif
                                                @endforeach
                                            @else
                                                Без баннеров
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-warning btn-edit" title="Редактировать"
                                               href="{{route('banner-groups.edit', $group->id)}}"><i
                                                        class="fa fa-pencil" aria-hidden="true"></i></a>
                                            @if(Route::currentRouteName() == 'banner-groups.trashed')
                                                <button class="btn btn-success btn-restore" data-id="{{$group->id}}"><i
                                                            class="fa fa-level-up"
                                                            aria-hidden="true" title="Восстановить"></i></button>
                                            @endif
                                            <button class="btn btn-danger btn-delete" data-id="{{$group->id}}"><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true" title="Удалить"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Включенные баннеры</th>
                                    <th>Управление</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('modals')
    <div class="modal modal-danger fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Удаление группы</h4>
                </div>
                <div class="modal-body">
                    @if(Route::currentRouteName() == 'banner-groups.trashed')
                        <p>Вы действительно хотите удалить группу? Баннеры группы удалены не будут.</p>
                    @else
                        <p>Вы действительно хотите убрать группу в корзину?</p>
                    @endif
                    <input type="hidden" class="delete-group-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-outline btn-remove">Убрать</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-success fade" id="modal-restore">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Восстановление группы</h4>
                </div>
                <div class="modal-body">
                    <p>Вы действительно хотите восстановить группу?</p>
                    <input type="hidden" class="restore-group-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-outline btn-restore-group">Восстановить</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- page script -->
    <script>

        $(".btn-delete").click(function () {
            $('.delete-group-id').val($(this).data('id'));
            $('#modal-delete').modal('show');
        });

        $(".btn-remove").click(function () {
            var id = $('.delete-group-id').val();
            var sendAr = {
                _method: 'DELETE',
                _token: $('.token').val()
            };
            $.post('/banner-groups/' + id, sendAr, function (data) {
                console.log(data);
                $('tr[data-id="' + id + '"]').fadeOut(600);
            });
            $('#modal-delete').modal('hide');
        });

        @if(Route::currentRouteName() == 'banner-groups.trashed')
        $(".btn-restore").click(function () {
            $('.restore-group-id').val($(this).data('id'));
            $('#modal-restore').modal('show');
        });

        $(".btn-restore-group").click(function () {
            var id = $('.restore-group-id').val();
            var sendAr = {
                _token: $('.token').val()
            };
            $.post('/banner-groups/' + id + '/restore', sendAr, function (data) {
                $('tr[data-id="' + id + '"]').fadeOut(600);
            });
            $('#modal-restore').modal('hide');
        });
        @endif

        $(function () {
            $('#main-table').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection