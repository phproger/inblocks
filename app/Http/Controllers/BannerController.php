<?php

namespace App\Http\Controllers;

use Image;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\BannerGroup;
use function MongoDB\BSON\toJSON;


class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.banner.list', [
            'banners' => Banner::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.banner.form', [
            'banner' => new Banner(),
            'bannerGroups' => BannerGroup::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.banner.form', [
            'banner' => Banner::find($id),
            'bannerGroups' => BannerGroup::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == 0){
            $banner = new Banner ();
        } else {
            $banner = Banner::find($id);
        }
        $banner->fill($request->all());
        $banner->save();

        if (isset($request->groups)) {
            $banner->bannerGroups()->sync($request->groups);
        }

        if($request->image) {
            $image = Image::make($request->image);
            unlink($request->image);
            $filepath = 'banners_images/banner_'.$banner->id.'.jpg';
            $image->interlace()->save($filepath);
            $banner->image = $filepath;
        }
       
        $banner->save();

        return redirect(route('banners.edit', $banner->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::withTrashed()->where('id', $id)->first();
        if (!$banner->trashed()) {
            $banner->delete();
        } else {
            unlink('../public/' . $banner->image);
            $banner->forceDelete();

        }

    }

    /**
     * Display a listing of the deleted banners
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        return view('pages.banner.list', [
            'banners' => Banner::onlyTrashed()->get()]);
    }

    /**
     * Restore deleted banner.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $banner = Banner::withTrashed()->where('id', $id)->first();
        $banner->restore();
    }
}