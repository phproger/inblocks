<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Test</h1>
<div class="inblocks">
    <a href="">
        <img src="" alt="" width="700">
    </a>
</div>
<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>

<script>
    $.get('http://inblocks.karpenko.tech/api/USER_KEY/GROUP_ID', function(data){
        window.inblocks = data;
        window.cur_inblock = window.inblocks.length-1;
        RotateInblocks();

    }, 'json');

    function RotateInblocks() {


        if (window.cur_inblock === window.inblocks.length-1) window.cur_inblock = 0;
        else window.cur_inblock += 1;
        $('.inblocks a').attr('href', window.inblocks[window.cur_inblock].url);
        $('.inblocks a img').attr('src','http://inblocks.karpenko.tech/' + window.inblocks[window.cur_inblock].image);
        window.setTimeout('RotateInblocks()',window.inblocks[window.cur_inblock].pivot.time * 1000);
    }

</script>
</body>
</html>