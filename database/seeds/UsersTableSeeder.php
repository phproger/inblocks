<?php

use \Webpatser\Uuid\Uuid;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws
     */
    public function run()
    {
        DB::table('users')->insert([
      'name' => 'Vladimir',
      'email' => 'dev.vladimirkarpenko@gmail.com',
      'password' => bcrypt('karpenko'),
      'key' => Uuid::generate()->string,
      'role_id' => 1
    ]);
    }
}
