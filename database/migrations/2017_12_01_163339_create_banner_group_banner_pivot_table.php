<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerGroupBannerPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_group_banner', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_group_id')->unsigned()->index();
            $table->foreign('banner_group_id')->references('id')->on('banner_groups')->onDelete('cascade');
            $table->integer('banner_id')->unsigned()->index();
            $table->foreign('banner_id')->references('id')->on('banners')->onDelete('cascade');
            $table->integer('time')->default(5);
            $table->integer('order')->default(1);
            $table->dateTime('show_till')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banner_group_banner');
    }
}
