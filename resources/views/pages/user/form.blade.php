@extends('common.index')

@section('styles')

@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if($user->id)
                    Настройки пользователя {{$user->name}}
                @else
                    Создание пользователя
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                @if(Auth::user()->role->role == 'Admin')
                    <li><a href="{{route('users.index')}}">Список пользователей</a></li>
                @endif
                @if($user->id)
                    <li class="active">{{$user->name}}</li>
                @else
                    <li class="active">Создание</li>
                @endif
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="/img/user2-160x160.jpg" alt="User profile picture">

                            <h3 class="profile-username text-center">{{$user->name}}</h3>

                            <p class="text-muted text-center">{{$user->role->readable()}}</p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Всего баннеров (уник.)</b> <a class="pull-right">{{ $user->banners->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Неактивные</b> <a class="pull-right">{{ $user->unactiveBanners()->count() }}</a>
                                </li>
                            </ul>

                            <button class="btn btn-danger btn-block btn-delete"><b>Удалить</b></button>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#settings" data-toggle="tab">Настройки</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="settings">
                                <form class="form-horizontal" method="POST" action="{{route('users.update', $user->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">Имя</label>
                                        <div class="col-sm-10">
                                            <input type="name" class="form-control" id="inputName" placeholder="Имя" name="name" value="{{$user->name}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="{{$user->email}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputPassword" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPasswordAgain" class="col-sm-2 control-label">Подтверждение пароля</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputPasswordAgain" name="password_again">
                                        </div>
                                    </div>
                                    @if(Auth::user()->role->role == 'Admin')
                                        <div class="form-group">
                                            <label for="inputRole" class="col-sm-2 control-label">Роль пользователя</label>
                                            <div class="col-sm-10">
                                                <select name="role_id" class="form-control" id="">
                                                    @foreach($roles as $role)
                                                        <option value="{{$role->id}}"
                                                            @if($role->id == $user->role_id)
                                                                selected
                                                            @endif>{{$role->readable()}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="inputPasswordAgain" class="col-sm-2 control-label">API ключ</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputKey" name="key" readonly value="{{$user->key}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-offset-2 col-xs-2">
                                            <button type="button" class="btn btn-success generate-key">Сгенерировать ключ</button>
                                        </div>
                                        <div class="col-xs-offset-6 col-xs-2">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('modals')
    <div class="modal modal-danger fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Удаление пользователя</h4>
                </div>
                <div class="modal-body">
                        <p>Вы действительно хотите удалить пользователя?
                           Действие необратимо, вместе с пользователем будут удалены его блоки</p>
                    <input type="hidden" class="delete-banner-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Закрыть</button>
                    <a href="{{route('users.delete', $user->id)}}" class="btn btn-outline btn-remove">Удалить</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script>

        $(".btn-delete").click(function () {
            $('#modal-delete').modal('show');
        });

        $(".generate-key").click(function () {
            $.post('{{route('users.generate_key')}}',{_token:'{{csrf_token()}}'}, function (data) {
                $("#inputKey").val(data);
            });
        });
    </script>
@endsection