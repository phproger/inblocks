<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannersGroups extends Model
{
    public $table = 'banner_group_banner';
    public $timestamps = false;
}
