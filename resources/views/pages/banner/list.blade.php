@extends('common.index')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Список баннеров
                @if(Route::currentRouteName() == 'banners.trashed')
                    <a href="{{route('banners.index')}}">
                        <small>Корзина</small>
                    </a>
                @else
                    <a href="{{route('banners.trashed')}}">
                        <small>Все</small>
                    </a>
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                @if(Route::currentRouteName() != 'banners.trashed')
                    <li class="active">Список баннеров</li>
                @else
                    <li><a href="{{route('banners.index')}}">Список баннеров</a></li>
                    <li class="active">Корзина</li>
                @endif
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!--  <div class="box-header">
                        </div> -->
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" class="token" value="{{csrf_token()}}">
                            <table id="main-table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Миниатюра</th>
                                    <th>Название</th>
                                    <th>Группа баннеров</th>
                                    <th>Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
                                    <tr data-id="{{$banner->id}}">
                                        <td>{{$banner->id}}</td>
                                        <td>
                                            @if($banner->image)
                                                <img class="banner-miniature img-responsive img-square"
                                                     src="/{{$banner->image}}" alt="Изображение баннера">

                                            @else
                                                <img class="banner-miniature img-responsive img-square"
                                                     src="/img/boxed-bg.jpg" alt="У баннера нет изображения">

                                            @endif
                                        </td>
                                        <td>{{$banner->name}}</td>
                                        <td>
                                            @if($banner->bannerGroups())
                                                @foreach($banner->bannerGroups()->get() as $group)
                                                    <a href="{{route('banner-groups.edit', $group->id)}}"
                                                       target="_blank">{{$group->name}} </a>  @if(!$loop->last) | @endif
                                                @endforeach
                                            @else
                                                Без групп
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-warning btn-edit" title="Редактировать"
                                               href="{{route('banners.edit', $banner->id)}}"><i
                                                        class="fa fa-pencil" aria-hidden="true"></i></a>
                                            @if(Route::currentRouteName() == 'banners.trashed')
                                            <button class="btn btn-success btn-restore" data-id="{{$banner->id}}"><i
                                                        class="fa fa-level-up"
                                                        aria-hidden="true" title="Восстановить"></i></button>
                                            @endif
                                            <button class="btn btn-danger btn-delete" data-id="{{$banner->id}}"><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true" title="Удалить"></i>
                                            </button>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Миниатюра</th>
                                    <th>Название</th>
                                    <th>Группа баннеров</th>
                                    <th>Управление</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('modals')
    <div class="modal modal-danger fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Удаление баннера</h4>
                </div>
                <div class="modal-body">
                    @if(Route::currentRouteName() == 'banners.trashed')
                    <p>Вы действительно хотите удалить баннер? Изображение так же будет удалено.</p>
                    @else
                    <p>Вы действительно хотите убрать баннер в корзину?</p>
                    @endif
                    <input type="hidden" class="delete-banner-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-outline btn-remove">Убрать</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-success fade" id="modal-restore">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Восстановление баннера</h4>
                </div>
                <div class="modal-body">
                        <p>Вы действительно хотите восстановить баннер?</p>
                    <input type="hidden" class="restore-banner-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-outline btn-restore-banner">Восстановить</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- page script -->
    <script>


        $(".btn-delete").click(function () {
            $('.delete-banner-id').val($(this).data('id'));
            $('#modal-delete').modal('show');
        });

        $(".btn-remove").click(function () {
            var id = $('.delete-banner-id').val();
            var sendAr = {
                _method: 'DELETE',
                _token: $('.token').val()
            };
            $.post('/banners/' + id, sendAr, function (data) {
                console.log(data);
                $('tr[data-id="' + id + '"]').fadeOut(600);
            });
            $('#modal-delete').modal('hide');
        });

        @if(Route::currentRouteName() == 'banners.trashed')
        $(".btn-restore").click(function () {
            $('.restore-banner-id').val($(this).data('id'));
            $('#modal-restore').modal('show');
        });

        $(".btn-restore-banner").click(function () {
            var id = $('.restore-banner-id').val();
            var sendAr = {
                _token: $('.token').val()
            };
            $.post('/banners/' + id + '/restore', sendAr, function (data) {
                $('tr[data-id="' + id + '"]').fadeOut(600);
            });
            $('#modal-restore').modal('hide');
        });
        @endif

        $(function () {
            $('#main-table').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection