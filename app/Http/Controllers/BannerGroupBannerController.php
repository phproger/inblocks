<?php

namespace App\Http\Controllers;

use App\Models\BannersGroups;
use Illuminate\Http\Request;

class BannerGroupBannerController extends Controller
{
    
    /**
     * SAVE ORDER
     */
    public function saveBannersOrder(Request $request, $group_id) {

        if (!$request->bannersOrder || !$group_id)
            return response()->json(['error' => 'Incorrect request']);

        $order = 0;
        foreach($request->bannersOrder as $banner_id) {

            $relation = BannersGroups::where('banner_id', $banner_id)->where('banner_group_id', $group_id)->first();
            $relation->order = $order;
            $relation->save();

            $order++;
        }

        return response()->json(['ok']);
    }

     /**
     * UPDATE RELATION INFO
     */
    public function saveBannerData(Request $request, $group_id) {

        if (!$group_id)
            return response()->json(['error' => 'Incorrect request']);

        $relation = BannersGroups::where('banner_id', $request->id)->where('banner_group_id', $group_id)->first();
      	$relation->time = $request->time;
      	$relation->show_till = $request->show_till;
        $relation->save();

        return response()->json(['ok']);
    }
}
