<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\BannerGroup;
use App\Models\User;
use App\Models\Role;

use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.user.form', [
            'user' => User::find($id),
            'roles' => Role::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == 0){
            $user = new Banner ();
        } else {
            $user = User::find($id);
        }

        if(!is_null($request->password) && $request->password == $request->password_again) {
            $user->password = bcrypt($request->password);
        } else {
            unset($request['password']);
        }

        $user->fill($request->all());


        $user->save();

        return redirect(route('users.edit', $user->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        foreach (Banner::where('user_id', $id)->get() as $banner) {
            unlink('../public/' . $banner->image);
            $banner->forceDelete();
        }

        foreach (BannerGroup::where('user_id', $id)->get() as $group) {
            $group->forceDelete();
        }

        $user->forceDelete();
        if($id == Auth::user()->id) {
            return redirect(route('logout.get'));
        } else {
            return redirect(route('homepage'));
        }
    }

    /**
     * Generate new api key.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateKey()
    {
        echo Uuid::uuid4()->toString();
    }
}
