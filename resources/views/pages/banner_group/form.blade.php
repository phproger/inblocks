@extends('common.index')

@section('styles')
    <link rel="stylesheet" href="/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if($group->id)
                    {{$group->name}}
                @else
                    Создание группы
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('banner-groups.index')}}">Список групп баннеров</a></li>
                @if($group->id)
                    <li class="active">{{$group->name}}</li>
                @else
                    <li class="active">Создание</li>
                @endif
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <!-- <div class="box-header">
                        </div> -->
                        <div class="box-body">
                            <!-- Date -->
                            <form class="col-md-12" action="{{route('banner-groups.update', $group->id? $group->id:0)}}"
                                  method="POST" enctype="multipart/form-data" name="group-editor" id="group-editor">

                                <div class="form-group col-md-6 col-xs-12">
                                    <label for="bannerName">Hазвание группы</label>
                                    <input type="text" class="form-control" id="bannerName" name="name"
                                           value="{{$group->name}}" required>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <br>
                                    <p class="text-light-blue">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        На данный момент, баннеры добавляются в группу при их редактировании</p>
                                </div>
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="_method" value="PUT">
                                {{--<input type="hidden" name="id" value="{{$group->id}}">--}}
                                @if(!empty($group->id))
                                <table id="main-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Время показа (сек.)</th>
                                        <th>Отображать до</th>
                                    </tr>
                                    </thead>
                                    <tbody class="bannerRows">
                                    @foreach($group->banners()->orderBy('pivot_order', 'asc')->get() as $banner)
                                        <tr class="bannerRow" data-id="{{$banner->id}}">
                                            <td>
                                                {{$banner->name}}
                                            </td>
                                            <td>
                                                <input class="form-control time" type="number" min="1" max="100"
                                                       value="{{$banner->pivot->time}}">
                                            </td>
                                            <td>
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right show_till"
                                                           value="{{$banner->pivot->show_till}}">
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Название</th>
                                        <th>Время показа (сек.)</th>
                                        <th>Отображать до</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                @endif
                            </form>
                            <input type="submit" class="btn btn-primary" value="Сохранить" form="group-editor">
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('scripts')
    <script src="/bower_components/moment/min/moment.min.js"></script>

    <script src="/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>


    <script type="text/javascript">
        $(".bannerRows").sortable({
            stop: function () {
                var bannersOrder = [];
                $('.bannerRow').each(function () {
                    bannersOrder.push($(this).attr('data-id'));
                });

                bannersOrder.pop();
                $.post('{{ route('save-banners-order', $group->id) }}', {
                    bannersOrder: bannersOrder,
                    _token: '{{ csrf_token() }}'
                }, 'json');
            }
        });
        $(".bannerRow").change(function () {
            var sendAr = {
                _token: '{{ csrf_token() }}',
                id: $(this).attr('data-id'),
                time: $(this).find('.time').val(),
                show_till: $(this).find('.show_till').val()
            };

            $.post('{{ route('save-banner-data', $group->id) }}',
                sendAr, 'json');
        });

        //Date picker
        $('.date').datetimepicker({
            format: "Y-MM-DD HH:mm:ss"
        })

        $(".date").on("dp.change", function(e) {
            $(this).closest('.bannerRow').trigger('change');
        });

    </script>
@endsection