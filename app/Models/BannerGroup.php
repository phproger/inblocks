<?php

namespace App\Models;

use Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerGroup extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'user_id'];
    protected $dates =  ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function banners()
    {
        return $this->belongsToMany('App\Models\Banner', 'banner_group_banner')->withPivot('time', 'show_till', 'order');
    }

    public function api_banners()
    {
        return $this->belongsToMany('App\Models\Banner', 'banner_group_banner')
            ->withPivot('time', 'show_till', 'order')
            ->where('banner_group_banner.show_till', null)
            ->orWhere('banner_group_banner.show_till', '>', Date::now())
            ->orderBy('banner_group_banner.order', 'asc');
    }

}
