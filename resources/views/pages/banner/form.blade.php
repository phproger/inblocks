@extends('common.index')

@section('styles')

@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if($banner->id)
                    {{$banner->name}}
                @else
                    Создание баннера
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('banners.index')}}">Список баннеров</a></li>
                @if($banner->id)
                    <li class="active">{{$banner->name}}</li>
                @else
                    <li class="active">Создание</li>
                @endif
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <!-- <div class="box-header">
                        </div> -->
                        <div class="box-body">
                            <!-- Date -->
                            <form class="col-md-6" action="{{route('banners.update', $banner->id? $banner->id:0)}}"
                                  method="POST" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="bannerName">Hазвание баннера</label>
                                    <input type="text" class="form-control" id="bannerName" name="name"
                                           value="{{$banner->name}}" required>
                                </div>

                                <div class="form-group">
                                    <label for="bannerUrl">Ссылка баннера</label>
                                    <input type="text" class="form-control" id="bannerUrl" name="url"
                                           value="{{$banner->url}}" required>
                                </div>

                                <div class="form-group">
                                    <label>Группы баннеров</label>
                                    <select class="form-control" multiple name="groups[]">
                                        @foreach($bannerGroups as $group)
                                            <option value="{{$group->id}}"
                                                    @if($banner->bannerGroups->contains($group->id))
                                                    selected
                                                    @endif>

                                                {{$group->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Загрузить баннер</label>
                                    <input type="file" class="form-control" class="btn btn-primary" name="image"
                                    @if(empty($banner->image))
                                           required
                                    @endif>
                                </div>

                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="id" value="{{$banner->id}}">
                                <input type="submit" class="btn btn-primary" value="Сохранить">
                            </form>

                            <div class="col-md-6">
                                @if($banner->image)
                                    <img src="/{{$banner->image}}" style="max-height: 304px;margin: 0 auto;">
                                @else
                                    <img src="https://s9.stc.all.kpcdn.net/share/i/12/10065254/inx960x640.jpg"
                                         style="max-height: 304px;margin: 0 auto;">
                                @endif
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('scripts')
@endsection