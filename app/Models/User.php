<?php

namespace App\Models;

use Date;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'key'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function bannerGroups()
    {
        return $this->hasMany('App\Models\BannerGroup');
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Banner');
    }

    public function unactiveBanners()
    {
        return $this->banners->where('banner_group_banner.show_till', '<', Date::now());
    }
}
