<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function readable()
    {
        $roles = ['Admin' => 'Администратор', 'User' => 'Пользователь'];
        return $roles[$this->role];
    }
}
