<?php

namespace App\Http\Controllers;

use App\Models\BannersGroups;
use App\Models\BannerGroup;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getGroup($user_key, $group_id)
    {

        if (!is_null(BannerGroup::find($group_id))) {
            if (BannerGroup::find($group_id)->user->key == $user_key) {


                return response(BannerGroup::find($group_id)->api_banners->toJson())->header('Access-Control-Allow-Origin', '*');
            } else {
                return response()->json(['error' => 'Wrong user key']);
            }
        } else {
            return response()->json(['error' => 'Wrong group id']);
        }
    }
}
